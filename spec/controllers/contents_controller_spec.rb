require 'rails_helper'

RSpec.describe ContentsController, type: :controller do

  describe "GET #top" do
    it "returns http success" do
      get :top
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #list" do
    it "returns http success" do
      get :list
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #article" do
    it "returns http success" do
      get :article
      expect(response).to have_http_status(:success)
    end
  end

end
