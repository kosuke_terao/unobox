module ActiveRecord
  module ConnectionAdapters # :nodoc:
    module SchemaStatements
      MAX_INDEX_LENGTH_FOR_UTF8MB4 = 191
      def initialize_schema_migrations_table
        if @config[:encoding] == 'utf8mb4'   
          ActiveRecord::SchemaMigration.create_table(MAX_INDEX_LENGTH_FOR_UTF8MB4)
        else
          ActiveRecord::SchemaMigration.create_table
        end
      end
    end
  end
end
module ActiveRecord            
  class SchemaMigration < ActiveRecord::Base
    def self.index_name
      "#{Base.table_name_prefix}unique_schema_migrations#{Base.table_name_suffix}"
    end

    def self.create_table(limit=nil)
      unless connection.table_exists?(table_name)
        version_options = {null: false}
        version_options[:limit] = limit if limit

        connection.create_table(table_name, id: false) do |t|
          t.column :version, :string, version_options
        end
        connection.add_index table_name, :version, unique: true, name: index_name
      end
    end               
  end
end
ActiveSupport.on_load :active_record do
  module ActiveRecord::ConnectionAdapters
 
    class AbstractMysqlAdapter
      def create_table_with_innodb_row_format(table_name, options = {})
        table_options = options.merge(:options => 'ENGINE=InnoDB ROW_FORMAT=DYNAMIC')
        create_table_without_innodb_row_format(table_name, table_options) do |td|
          yield td if block_given?
        end
      end
      alias_method_chain :create_table, :innodb_row_format
    end
 
  end
end
