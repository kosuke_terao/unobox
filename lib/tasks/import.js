function Sleep( T ){ 
   var d1 = new Date().getTime(); 
   var d2 = new Date().getTime(); 
   while( d2 < d1+1000*T ){    //T秒待つ 
       d2=new Date().getTime(); 
   } 
   return; 
} 
//Sleep(Math.round(Math.random() * 7200));//1秒待つ

var fs = require('fs');


huntedUrls = ['https://www.instagram.com/unobox/', 
'https://www.instagram.com/unobox/?max_id=1250496585216894710',
'https://www.instagram.com/unobox/?max_id=1230392985195193423',
'https://www.instagram.com/unobox/?max_id=1209798672157583043',
'https://www.instagram.com/unobox/?max_id=1196787981674948159',
'https://www.instagram.com/unobox/?max_id=1177506837439784101',
'https://www.instagram.com/unobox/?max_id=1165202714535208205',
'https://www.instagram.com/unobox/?max_id=1154212565286463280',
'https://www.instagram.com/unobox/?max_id=1141270772286825910',
'https://www.instagram.com/unobox/?max_id=1128910279823926313',
'https://www.instagram.com/unobox/?max_id=1115928831718401203',
'https://www.instagram.com/unobox/?max_id=1102226644798613490',
'https://www.instagram.com/unobox/?max_id=918526306984993836',
//2ブロック
'https://www.instagram.com/unobox/?max_id=1259963783522100224',
'https://www.instagram.com/unobox/?max_id=1243322732338292707',
'https://www.instagram.com/unobox/?max_id=1218832878678385976',
'https://www.instagram.com/unobox/?max_id=1202982688209839232',
'https://www.instagram.com/unobox/?max_id=1185837570579310377',
'https://www.instagram.com/unobox/?max_id=1171689371228776025',
'https://www.instagram.com/unobox/?max_id=1159768272173868223',
'https://www.instagram.com/unobox/?max_id=1149003261554010073',
'https://www.instagram.com/unobox/?max_id=1135473538500732140',
'https://www.instagram.com/unobox/?max_id=1121740073498388096',
'https://www.instagram.com/unobox/?max_id=1109462227606381852',
'https://www.instagram.com/unobox/?max_id=1080295997272302845'
]; 
//urlMap = JSON.parse(fs.read('lib/tasks/articles.json'));
urlMap = {};

page = require('webpage').create();
page.onConsoleMessage = function(msg) {
    console.log(msg);
}

page.onError = function(msg, trace) {

  var msgStack = ['ERROR: ' + msg];

  if (trace && trace.length) {
    msgStack.push('TRACE:');
    trace.forEach(function(t) {
      msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function +'")' : ''));
    });
  }
  console.error(msgStack.join('\n'));

};

startFunc = function(){
  var huntedUrl = huntedUrls.shift();
  if(!huntedUrl){
    return phantom.exit();
  }
	console.log('url', huntedUrl);

  if(urlMap[huntedUrl] && urlMap[huntedUrl].size > 0 ){
	  console.log('already crawled', huntedUrl);
    return startFunc();
  }
  page.open(huntedUrl, function(status){
    Sleep(2);
    try{
	    var urls = page.evaluate(function() {
        //huntedUrls = follow_targets;
        var targets = [];
        var maxId;
	    	$('article').find('a').each(function(){
          var link = $(this).attr('href');
          console.log('link=', link);
          
          //詳細ページを取得
          if(/\/p\/.+/.test(link) && !/undefined/.test(link)){
            var tmpurl = 'https://www.instagram.com' + link;
            targets.push(tmpurl);
          }

          /*次のページを取得
          var matches = link.match(/max_id=(\d+)/);
          if(matches){
            maxId = matches[1];
          }
          if (maxId) {
            console.log('max_id =', maxId);
          }
          */
        });
        /*
        var nextUrl = 'https://www.instagram.com/unobox/?max_id=' + maxId ;
        console.log('next url', nextUrl);
		  	return {articles: targets, nextUrl: nextUrl};
        */
        return targets;
		  });
      //console.log(urls.toSource());
      //huntedUrls.push(urls.nextUrl);
      //showUrls.length = 0;
      if(urlMap[huntedUrl]){
        urlMap[huntedUrl].length = 0;
      }else{
        urlMap[huntedUrl] = [];
      }
      urls.forEach(function(v,i){urlMap[huntedUrl].push(v)});
      //assertFunc();
      fs.write('lib/tasks/articles.json', JSON.stringify(urlMap), 'w');
      startFunc();
      //phantom.exit();
    }catch(e){
      console.log(e);
      phantom.exit();
    }
	});
};

assertFunc = function(){
  var showUrl = showUrls.shift();
  if(!showUrl){
    //return startFunc();
    return phantom.exit();
  }
  console.log('対象url', showUrl);
  page.open(showUrl, function(){
    //Sleep(1.5);
    try{
      var response = page.evaluate(function() {
        //return document.getElementsByTagName('html')[0].innerHTML;
        var res = {}
        res.image = $('div._jjzlb').find('img').attr('src');
        res.time = $('time').attr('datetime');
        res.body = $('span[title=編集済み]').text()
        return res;
      });
      console.log('image', response.image);
      console.log('time', response.time);
      console.log('body', response.body);
    }catch(e){
      console.log(e);
      phantom.exit();
    }
    assertfunc();
    //phantom.exit();
    /*
    window.setTimeout(
      assertFunc,
      Math.round(2000 + Math.random() * 2000)
    );
    */
  });
};


(function () {
	startFunc();
})();



