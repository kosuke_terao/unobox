class Tasks::Base
  def run
    begin
      execute
    rescue Exception => e
      #TwitterObserver.notify("#{e.message}\n#{e.backtrace}") 
      #AdminMailer.notify("#{e.message}\n#{e.backtrace}").deliver
      raise e
    end
  end
end

