class Tasks::ImportArticle < Tasks::Base
  def execute
    File.open("lib/tasks/articles2.json", "r") do |f|
      article_data = JSON.load(f)
      article_data.each do |url, data|
        time = Chronic.parse(data["time"])
        Article.find_or_create_by(instagram_post_id: url ) do |a|
          a.body = data["body"]
          a.post_date = time
          a.image_url = data["image"]
          a.is_public = true
        end
      end
    end
  end
end
Tasks::ImportArticle.new.run

