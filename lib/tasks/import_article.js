function Sleep( T ){ 
   var d1 = new Date().getTime(); 
   var d2 = new Date().getTime(); 
   while( d2 < d1+1000*T ){    //T秒待つ 
       d2=new Date().getTime(); 
   } 
   return; 
} 
//Sleep(Math.round(Math.random() * 7200));//1秒待つ

var fs = require('fs');
var args = phantom.args;

//相対パス
showUrl =  args[0];
articleMap = JSON.parse(fs.read('lib/tasks/articles2.json'));


//urlMap = JSON.parse(fs.read('lib/tasks/articles.json'));

page = require('webpage').create();
page.onConsoleMessage = function(msg) {
    console.log(msg);
}

page.onError = function(msg, trace) {

  var msgStack = ['ERROR: ' + msg];

  if (trace && trace.length) {
    msgStack.push('TRACE:');
    trace.forEach(function(t) {
      msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function +'")' : ''));
    });
  }
  console.error(msgStack.join('\n'));

};


startFunc = function(){
  if(!showUrl){
    //return startFunc();
    return phantom.exit();
  }
  console.log('対象url', showUrl);
  page.open(showUrl, function(){
    //Sleep(1.5);
    try{
      var response = page.evaluate(function() {
        //return document.getElementsByTagName('html')[0].innerHTML;
        var res = {}
        var $img = $('div._jjzlb').find('img');
        res.image = $img.attr('src');
        res.time = $('time').attr('datetime');
        res.body = $img.attr('alt');
        return res;
      });
      console.log('image', response.image);
      console.log('time', response.time);
      console.log('body', response.body);
      articleMap[showUrl] = response;
      fs.write('lib/tasks/articles2.json', JSON.stringify(articleMap), 'w');
    }catch(e){
      console.log(e);
      phantom.exit();
    }
    phantom.exit();
    /*
    window.setTimeout(
      assertFunc,
      Math.round(2000 + Math.random() * 2000)
    );
    */
  });
};


(function () {
	startFunc();
})();




