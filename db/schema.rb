# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160924154228) do

  create_table "article_tags", force: :cascade do |t|
    t.integer  "article_id", limit: 4
    t.integer  "tag_id",     limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "article_tags", ["article_id"], name: "index_article_tags_on_article_id", using: :btree
  add_index "article_tags", ["tag_id"], name: "index_article_tags_on_tag_id", using: :btree

  create_table "articles", force: :cascade do |t|
    t.text     "title",               limit: 65535
    t.text     "body",                limit: 65535,                 null: false
    t.datetime "post_date",                                         null: false
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.string   "image_file_name",     limit: 255
    t.string   "image_content_type",  limit: 255
    t.integer  "image_file_size",     limit: 4
    t.datetime "image_updated_at"
    t.string   "instagram_post_id",   limit: 255
    t.text     "image_url",           limit: 65535
    t.boolean  "is_public",                         default: false, null: false
    t.text     "body_for_display",    limit: 65535
    t.string   "image2_file_name",    limit: 255
    t.string   "image2_content_type", limit: 255
    t.integer  "image2_file_size",    limit: 4
    t.datetime "image2_updated_at"
    t.string   "image3_file_name",    limit: 255
    t.string   "image3_content_type", limit: 255
    t.integer  "image3_file_size",    limit: 4
    t.datetime "image3_updated_at"
    t.text     "body2",               limit: 65535
    t.text     "body3",               limit: 65535
  end

  add_index "articles", ["instagram_post_id"], name: "index_articles_on_instagram_post_id", using: :btree
  add_index "articles", ["post_date"], name: "index_articles_on_post_date", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string   "name",             limit: 255,                   null: false
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.string   "name_en",          limit: 255,                   null: false
    t.string   "slug",             limit: 255,                   null: false
    t.text     "description",      limit: 65535
    t.boolean  "for_display",                    default: false, null: false
    t.boolean  "for_body_display",               default: true,  null: false
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree
  add_index "tags", ["slug"], name: "index_tags_on_slug", unique: true, using: :btree

  add_foreign_key "article_tags", "articles"
  add_foreign_key "article_tags", "tags"
end
