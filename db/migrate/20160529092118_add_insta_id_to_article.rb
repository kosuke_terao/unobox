class AddInstaIdToArticle < ActiveRecord::Migration
  def change
    add_column :articles, :instagram_post_id, :string
    add_index  :articles, :instagram_post_id, unique:true,  length: 191
  end
end
