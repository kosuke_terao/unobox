class AddNameEnToTags < ActiveRecord::Migration
  def change
    add_column :tags, :name_en, :string, null: false
    add_column :tags, :slug, :string, null: false
  end
end
