class AddPublicToArticle < ActiveRecord::Migration
  def change
    add_column :articles, :is_public, :boolean, null: false, default: false
  end
end
