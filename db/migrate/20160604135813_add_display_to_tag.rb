class AddDisplayToTag < ActiveRecord::Migration
  def change
    add_column :tags, :for_display, :boolean, null: false, default: false
  end
end
