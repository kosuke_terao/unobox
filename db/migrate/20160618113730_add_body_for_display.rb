class AddBodyForDisplay < ActiveRecord::Migration
  def change
    add_column :articles, :body_for_display, :text
  end
end
