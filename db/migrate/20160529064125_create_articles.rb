class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.text :title
      t.text :body, null:false
      t.datetime :post_date, null:false

      t.timestamps null: false
    end
    add_index :articles, :post_date
  end
end
