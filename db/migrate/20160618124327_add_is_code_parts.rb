class AddIsCodeParts < ActiveRecord::Migration
  def change
    add_column  :tags, :for_body_display,  :boolean, null: false, default: true
  end
end
