class Article < ActiveRecord::Base
  has_many :article_tags, dependent: :destroy
  has_many :tags, through: :article_tags

  after_save :set_tags
  before_save  :set_body_for_display
  before_create :set_body_for_display

  attr_accessor :twitter_post
  scope :recently, ->{where(is_public: true).order("post_date desc")}

  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  has_attached_file :image2, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  has_attached_file :image3, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  validates_attachment_content_type :image2, :content_type => /\Aimage\/.*\Z/
  validates_attachment_content_type :image3, :content_type => /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, attributes: [:image, :image2, :image3], :less_than => 2.megabytes
  
  TAG_REGEXP = /(?:#.+?\s|#.+?$)/


  def set_tags
    tag_seeds = body.scan(TAG_REGEXP)
    tags = tag_seeds.map{|t|t.gsub(/#|\s/, "")}
    tags.each do |tag_name|
      tag = Tag.find_or_create_by(name: tag_name) do |t|
        t.name_en = t.name
      end
      ArticleTag.find_or_create_by(article: self, tag: tag)
    end
  end
  def set_body_for_display
    self.body_for_display = body.gsub(TAG_REGEXP, "").gsub(/\*/, "").gsub(/\.\r\n/, "\r\n").gsub(/\.\n/, "\n")
  end

  def post_image_to_twitter
    DownloadImage.save_image(image_url, self.id.to_s, "images")
    dir_name = Rails.root.join("public").join("system").join("images")
    file_path = dir_name.join(self.id.to_s) 
    url = Rails.application.routes.url_helpers.article_url(self, host: "unobox-blog.net", protocol: :https) 
    surl = Bitly.client.shorten(url).short_url
    msg =  ApplicationController.helpers.truncate(body_for_display.to_str.html_safe , length: 80) +  " #{surl}"
    TwitterPost.post_with_media(file_path, msg)
  end

  def image_url_inside
    #"/system/review_images/#{image_fname}"
    if image.present?
      image.url
    else
      image_url
    end
  end
end
