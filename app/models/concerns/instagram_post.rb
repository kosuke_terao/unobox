class InstagramPost
  class << self
    def get_client(user=nil)
      client = Instagram.client(access_token: "2813144.562d504.d6eeeaf64c73488db56628865e3eacf4")
      return client
    end
    def post_with_media(image_path, msg_seed)
      msg = ApplicationController.helpers.truncate(msg_seed.to_str.html_safe, length: 110).to_str
      begin
        c = self.get_client
        c.update_with_media(msg,  File.open(image_path))
      rescue StandardError => e
        AdminMailer.silent_notify("#{e.message}\n#{e.backtrace}").deliver
      end
    end
    def crawle 
      articles = []
      c= self.get_client
      medias = c.user_recent_media(count: 100)
      medias.each do |media|
        article = Article.find_or_create_by(instagram_post_id: media.id) do |a|
          a.post_date = Time.at(media.created_time.to_i)
          #= media.tags
          a.body = media.caption.text
          a.image_url = media.images.standard_resolution.url
          a.is_public = true
          a.twitter_post = true
        end
        articles << article
      end
      articles
    end
  end
end
