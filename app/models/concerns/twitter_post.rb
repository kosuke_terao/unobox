class TwitterPost 
  class << self
    def get_client(user=nil)
      consumer_key        = "lXfQuK07W1OXuHse3kGdXnST3"
      consumer_secret     = "n2WSP24tAwX50GdBTjkOmTF82x18qwYBlbOt74F9tJYeLlSilU"
      client = ::Twitter::REST::Client.new do |config|
        config.consumer_key = consumer_key
        config.consumer_secret = consumer_secret
        if Rails.env.production?
          config.access_token        = "728958363787087872-cPMHxoJFmDVnIOAcVIuHAUWO510DgzO"
          config.access_token_secret = "TZ3v0UfBJwKzFn3IhgansnHIkx23aF5N21EjA2OwFpwZl"
        else
          config.access_token        = "3174234991-saZY5mogj8ZU8oLIYySMa143G8ss7qlCbfJ0oIH"
          config.access_token_secret = "fsxIE7dvPBUCOixyaxukZQqXgZBhcEe6VnhGR9L0ek45b"
        end
      end
      return client
    end
    def post(message)
      begin
        client = self.get_client
        msg = ApplicationController.helpers.truncate(message.to_str.html_safe , length: 140)
        client.update(msg.to_str)
      rescue StandardError => e
        AdminMailer.silent_notify("#{e.message}\n#{e.backtrace}").deliver
      end
    end
    def post_with_media(image_path, msg_seed)
      msg = ApplicationController.helpers.truncate(msg_seed.to_str.html_safe, length: 110).to_str
      begin
        c = self.get_client
        c.update_with_media(msg,  File.open(image_path))
      rescue StandardError => e
        AdminMailer.silent_notify("#{e.message}\n#{e.backtrace}").deliver
      end
    end
  end
end
