require "open-uri"
require 'resolv-replace'
require "timeout"

module DownloadImage
  class << self
    def save_image(url, fname, dir_name)
      # ready filepath
      dirName = Rails.root.join("public").join("system").join(dir_name)
      filePath = dirName.join(fname) 

      # create folder if not exist
      FileUtils.mkdir_p(dirName) unless FileTest.exist?(dirName)
      #return if FileTest.exist?(filePath)

      # write image adata
      begin
        open(filePath, 'wb') do |output|
          open(url) do |data|
            output.write(data.read)
          end
        end
      rescue => e
        puts "error #{e.message}"
        return false
      end
      return true
    end
  end
end
