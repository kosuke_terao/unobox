class Tag < ActiveRecord::Base
  include FriendlyId
  has_many :article_tags, dependent: :destroy
  has_many :articles, through: :article_tags
  has_many :related_articles, ->(tag){order("articles.post_date desc" )}, source: :article, through: :article_tags

  scope :for_top, ->{where("for_display = true").joins(:article_tags).group("tags.id").order("count(article_tags.id) desc")}
  scope :for_display, ->{where("for_display = true").order("name asc")}
  scope :dont_display, ->{where("for_display = false and for_body_display = true").order("name asc")}

  friendly_id :name_en, :use => [:slugged]

  def display_name
    name.gsub(/_/, " ")
  end

  def name_regexp
    name.split(/_/).join("|")
  end

  private

  def should_generate_new_friendly_id?
    slug.blank? || name_en_changed? || super
  end
end
