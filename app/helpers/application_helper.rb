module ApplicationHelper
  def render_calendar(date)
    as = Article.where("post_date between ? and ?", date.beginning_of_month, date.end_of_month).index_by{|a|a.post_date.strftime("%Y-%m-%d")}
    calendar(year: date.year, month: date.month, calendar_title: date.strftime("%Y/%m"), show_other_month: false,
             previous_month_text: link_to("<", calendar_path(date: date.prev_month.strftime("%Y-%m-%d")), class: 'change-month'), 
             next_month_text: link_to(">", calendar_path(date: date.next_month.strftime("%Y-%m-%d")), class: 'change-month') ) do |d|
      if as[d.strftime("%Y-%m-%d")]
        link_to d.day, all_list_path(date: d.strftime("%Y-%m-%d")), class: d.strftime("%a")
      else
        d.day
      end
    end
  end
end
