$(function(){
  if($('div.calendar').size() === 0){
    return
  }
  $.ajax({
    method: 'GET',
    url: '/calendar',
    dataType: 'html',
    success: function(data){
      $('div.calendar').html(data);
    }
  });
  $('div.calendar').on('click', '.change-month', function(){
    $.ajax({
      method: 'GET',
      url: $(this).attr('href'),
      dataType: 'html',
      success: function(data){
        $('div.calendar').html(data);
      }
    });
    return false;
  });
});
