class InstagramController < ApplicationController
  def index
    c= InstagramPost.get_client
    @medias = c.user_recent_media(count: 100)
  end
end
