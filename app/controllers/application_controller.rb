class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  ADMIN_USERS = { "pontan" => "world" }
  protect_from_forgery with: :exception
  add_crumb 'HOME', '/', except: :top

  private

  def authenticate
    authenticate_or_request_with_http_digest do |username|
      ADMIN_USERS[username]
    end
  end
end
