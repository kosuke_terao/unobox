class ContentsController < ApplicationController
  before_action :set_tags
  def top
    @articles = Article.recently.limit(10)
  end

  def list
    page = params[:page].to_i > 0 ? params[:page].to_i : 1
    @tag = Tag.friendly.find(params[:id])
    @articles = @tag.related_articles.page(page).per(20)
    add_crumb "#{@tag.display_name}"
  end
  def photo_list
    @tag = Tag.friendly.find(params[:id])
    @articles = @tag.related_articles
    add_crumb "#{@tag.display_name}の写真一覧"
  end
  def all_list
    page = params[:page].to_i > 0 ? params[:page].to_i : 1
    article_rel =Article.recently 
    if params[:date].present?
      date = Date.parse(params[:date])
      article_rel = article_rel.where("post_date between ? and ?", date.beginning_of_day, date.end_of_day)
      @title = "#{date.strftime("%Y/%-m/%-d")}の記事"
    end
    if params[:kw].present?
      @kw = params[:kw].to_s.strip
      article_rel = article_rel.where('title like ? or body like ?', "%#{@kw}%", "%#{@kw}%")
      @title = "キーワード「#{@kw}」を含む記事"
    end
    @articles = article_rel.page(page).per(10)
    @title ||="すべての記事 #{@articles.current_page}ページ"
    add_crumb @title 
  end
  def calendar
    if params[:date].present?
      @date = Date.parse(params[:date])
    else
      @date = Time.now
    end
    render layout: false
  end

  private
  def set_tags
    @tags = Tag.for_top

  end
end
