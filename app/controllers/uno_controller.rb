class UnoController < ApplicationController
  before_action :authenticate

  def index
  end
  
  def crawle_instagram
    InstagramPost.crawle
    redirect_to action: :index,  msg: "記事の取り込みが完了！"
  end

end
